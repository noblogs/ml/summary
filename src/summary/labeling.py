import importlib
from bertopic.representation import BaseRepresentation
from .postprocessing import postprocess_llm_topic


class _PostProcRepresentationModel(BaseRepresentation):
    """Post-process output from a BERTopic representation model."""

    def __init__(self, model):
        self._model = model

    def extract_topics(self, topic_model, documents, c_tf_idf, topics):
        topic_labels = self._model.extract_topics(
            topic_model, documents, c_tf_idf, topics)
        return dict(
            (key, [(postprocess_llm_topic(x), n) for x, n in value])
            for key, value in topic_labels.items())


def load_labeling_model(labeling_model_kind, **args):
    """Load a labeling model for topic representation."""

    build_fn = importlib.import_module(
        'summary.llm.' + labeling_model_kind).representation_model

    labeling_model, meta = build_fn(**args)

    return _PostProcRepresentationModel(labeling_model), meta
