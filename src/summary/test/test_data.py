from summary.data import Dataset


def test_dataset(tmp_path):
    data_path = str(tmp_path / 'foo')

    ds = Dataset()
    ds.metadata = {'value': 42}
    ds.posts = ['a', 'b', 'c']
    ds.save(data_path)

    ds2 = Dataset(data_path)
    value = ds2.metadata['value']
    assert value == 42
