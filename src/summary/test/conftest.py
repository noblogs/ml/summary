import pytest


@pytest.fixture
def test_docs():
    return [

        '''Oh I really want to talk about cheese. Cheese is the
        best!''',

        '''I have written a whole new encyclopedia on cheese.''',

        '''My favorite topic of conversation is cheese. I could talk
        about cheese all day long.''',

        '''The climate of the Arctic region is characterized by cold
        winters and cool summers. Its precipitation mostly comes in
        the form of snow and is low, with most of the area receiving
        less than 50 cm (20 in). High winds often stir up snow,
        creating the illusion of continuous snowfall. Average winter
        temperatures can go as low as −40 °C (−40 °F), and the coldest
        recorded temperature is approximately −68 °C (−90 °F). Coastal
        Arctic climates are moderated by oceanic influences, having
        generally warmer temperatures and heavier snowfalls than the
        colder and drier interior areas.''',

        '''Arctic vegetation is composed of plants such as dwarf
        shrubs, graminoids, herbs, lichens, and mosses, which all grow
        relatively close to the ground, forming tundra. An example of
        a dwarf shrub is the bearberry. As one moves northward, the
        amount of warmth available for plant growth decreases
        considerably. In the northernmost areas, plants are at their
        metabolic limits, and small differences in the total amount of
        summer warmth make large differences in the amount of energy
        available for maintenance, growth and reproduction. Colder
        summer temperatures cause the size, abundance, productivity
        and variety of plants to decrease. Trees cannot grow in the
        Arctic, but in its warmest parts, shrubs are common and can
        reach 2 m (6 ft 7 in) in height; sedges, mosses and lichens
        can form thick layers. In the coldest parts of the Arctic,
        much of the ground is bare; non-vascular plants such as
        lichens and mosses predominate, along with a few scattered
        grasses and forbs (like the Arctic poppy).''',

        '''During the Cretaceous time period, the Arctic still had
        seasonal snows, though only a light dusting and not enough to
        permanently hinder plant growth. Animals such as the
        Chasmosaurus, Hypacrosaurus, Troodon, and Edmontosaurus may
        have all migrated north to take advantage of the summer
        growing season, and migrated south to warmer climes when
        winter came. A similar situation may also have been found
        amongst dinosaurs that lived in Antarctic regions, such as the
        Muttaburrasaurus of Australia.''',

        '''Communism (from Latin communis, 'common, universal') is a
        sociopolitical, philosophical, and economic ideology within
        the socialist movement, whose goal is the creation of a
        communist society, a socioeconomic order centered around
        common ownership of the means of production, distribution, and
        exchange that allocates products to everyone in society based
        on need. A communist society would entail the absence of
        private property and social classes and ultimately money and
        the state (or nation state).''',

        '''Variants of communism have been developed throughout
        history, including anarchist communism, Marxist schools of
        thought, and religious communism, among others. Communism
        encompasses a variety of schools of thought, which broadly
        include Marxism, Leninism, and libertarian communism, as well
        as the political ideologies grouped around those. All of these
        different ideologies generally share the analysis that the
        current order of society stems from capitalism, its economic
        system, and mode of production, that in this system there are
        two major social classes, that the relationship between these
        two classes is exploitative, and that this situation can only
        ultimately be resolved through a social revolution. The two
        classes are the proletariat, who make up the majority of the
        population within society and must sell their labor power to
        survive, and the bourgeoisie, a small minority that derives
        profit from employing the working class through private
        ownership of the means of production. According to this
        analysis, a communist revolution would put the working class
        in power, and in turn establish common ownership of property,
        the primary element in the transformation of society towards a
        communist mode of production.''',

        '''Since the 1840s, the term communism has usually been
        distinguished from socialism. The modern definition and usage
        of the term socialism was settled by the 1860s, becoming
        predominant over alternative terms such as associationism
        (Fourierism), mutualism, or co-operative, which had previously
        been used as synonyms. Meanwhile, the term communism fell out
        of use during this period.''',

    ]
