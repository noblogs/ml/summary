from summary import clustering
from summary.embeddings import load_embedding_model, compute_embeddings
from summary.preprocessing import documents_to_sentences
from summary.labeling import load_labeling_model


class TestSummary():

    # Use a fast EN model for testing.
    EMBEDDING_MODEL = 'all-MiniLM-L12-v2'
    EMBEDDING_DIMENSIONS = 384

    LABELING_MODEL = 'flan'

    def test_embeddings(self, test_docs):
        corpus, docs_map = documents_to_sentences(
            test_docs,
            min_sentence_length=0,
            min_word_count=0)

        embedding_model = load_embedding_model(self.EMBEDDING_MODEL)
        embeddings = compute_embeddings(corpus, embedding_model)
        assert embeddings is not None

        # Verify expected shape of embeddings.
        expected_shape = (len(corpus), self.EMBEDDING_DIMENSIONS)
        assert embeddings.shape == expected_shape

        labeling_model, _ = load_labeling_model(self.LABELING_MODEL)
        assert labeling_model is not None

        topic_model, topics, _ = clustering.analyze_cluster(
            corpus,
            clustering.Params(
                umap_num_neighbors=10,
                umap_num_components=5,
                umap_min_dist=0.02,
                umap_metric='cosine',
                hdbscan_min_cluster_size=2,
            ),
            embedding_model,
            labeling_model,
            embeddings,
        )

        # Sanity check on the returned data.
        assert len(topics) == len(corpus)

        print(topic_model.get_topic_info())
        num_topics = len(topic_model.topic_sizes_)
        if topic_model._outliers:
            num_topics -= 1
        assert num_topics == 3
