import contextlib
import json
import numpy
import subprocess
from bertopic import BERTopic


class _JSONFile:

    def __init__(self, name):
        self.sfx = f'.{name}.json.zstd'

    def load(self, path):
        with open_compressed_file(path + self.sfx) as fd:
            return json.load(fd)

    def save(self, obj, path):
        with create_compressed_file(path + self.sfx) as fd:
            json.dump(obj, fd)


class _NumpyFile:

    def __init__(self, name):
        self.name = name
        self.sfx = f'.{name}.npz'

    def load(self, path):
        return numpy.load(path + self.sfx, mmap_mode='r')[self.name]

    def save(self, obj, path):
        kwargs = {self.name: obj}
        numpy.savez(path + self.sfx, **kwargs)


class _BERTopicModelFile:

    def load(self, path):
        return BERTopic.load(path + '.dat')

    def save(self, obj, path):
        obj.save(path + '.dat',
                 serialization='safetensors',
                 save_ctfidf=False)


class Dataset:

    FIELDS = {
        'topic_model': _BERTopicModelFile(),
        'metadata': _JSONFile('meta'),
        'corpus': _JSONFile('corpus'),
        'posts': _JSONFile('posts'),
        'posts_map': _JSONFile('posts_map'),
        'topic_posts_map': _JSONFile('topic_posts_map'),
        'embeddings': _NumpyFile('embeddings'),
        'probabilities': _NumpyFile('probabilities'),
    }

    def __init__(self, path=None, **kwargs):
        self._path = path
        self._values = dict(kwargs)

    def __getattr__(self, key):
        if key in self._values:
            return self._values[key]
        if key in self.FIELDS:
            value = self.FIELDS[key].load(self._path)
            self._values[key] = value
            return value
        raise AttributeError(key)

    def __setattr__(self, key, value):
        if key in self.FIELDS:
            self._values[key] = value
        else:
            super().__setattr__(key, value)

    def save(self, path=None):
        if not path:
            path = self._path
        if not path:
            raise Exception('No output path specified')
        for key, value in self._values.items():
            self.FIELDS[key].save(value, path)


@contextlib.contextmanager
def open_compressed_file(path):
    if path.endswith('.gz'):
        decomp = 'gzip'
    elif path.endswith('.zstd'):
        decomp = 'zstd'
    elif path.endswith('.xz'):
        decomp = 'xz'
    else:
        yield open(path)
    with subprocess.Popen(
            [decomp, '-dc', path],
            stdout=subprocess.PIPE) as pipe:
        yield pipe.stdout


@contextlib.contextmanager
def create_compressed_file(path):
    with open(path, 'wb') as outf:
        with subprocess.Popen(
                ['zstd', '-c'],
                stdin=subprocess.PIPE,
                stdout=outf,
                encoding='utf-8') as pipe:
            yield pipe.stdin
