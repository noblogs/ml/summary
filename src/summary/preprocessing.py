import re

import nltk
from nltk.tokenize import sent_tokenize


DEFAULT_MIN_SENTENCE_LENGTH = 30
DEFAULT_MIN_WORD_COUNT = 5

_word_rx = re.compile(r'\w{3,}')


def _count_words(s):
    return len(_word_rx.findall(s))


def _filter_sentence(min_sentence_length, min_word_count):
    def _f(s):
        return (
            (min_sentence_length == 0 or len(s) >= min_sentence_length) and
            (min_word_count == 0 or _count_words(s) >= min_word_count)
        )
    return _f


def documents_to_sentences(docs,
                           min_sentence_length=DEFAULT_MIN_SENTENCE_LENGTH,
                           min_word_count=DEFAULT_MIN_WORD_COUNT):
    """Split documents into multiple sentences.

    Returns the list of sentences, and a [document_index] ->
    [sentence_index] map as an array.
    """
    _init_nltk()

    filter_fn = _filter_sentence(min_sentence_length, min_word_count)
    sentences = [list(filter(filter_fn, sent_tokenize(doc))) for doc in docs]
    corpus = [sentence for doc in sentences for sentence in doc]
    docs_map = [i for i, doc in enumerate(sentences) for sentence in doc]

    return corpus, docs_map


def _init_nltk():
    nltk.download('punkt_tab')
