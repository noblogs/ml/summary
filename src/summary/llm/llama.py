import torch
from bertopic.representation import TextGeneration
from transformers import pipeline
from transformers import BitsAndBytesConfig, AutoModelForCausalLM, AutoTokenizer


LLAMA_LABELING_MODEL = 'meta-llama/Llama-2-13b-chat-hf'

# System prompt describes information given to all conversations
LLAMA_SYSTEM_PROMPT = """
<s>[INST] <<SYS>>
You are a helpful, respectful and honest assistant for labeling topics.
<</SYS>>
"""

# Example prompt demonstrating the output we are looking for
LLAMA_EXAMPLE_PROMPT = """
I have a topic that contains the following documents:
- Traditional diets in most cultures were primarily plant-based with a little meat on top, but with the rise of industrial style meat production and factory farming, meat has become a staple food.
- Meat, but especially beef, is the word food in terms of emissions.
- Eating meat doesn't make you a bad person, not eating meat doesn't make you a good one.

The topic is described by the following keywords: 'meat, beef, eat, eating, emissions, steak, food, health, processed, chicken'.

Based on the information about the topic above, please create a short label of this topic. Make sure you to only return the label and nothing more.
[/INST] Environmental impacts of eating meat
"""

# Our main prompt with documents ([DOCUMENTS]) and keywords ([KEYWORDS]) tags
LLAMA_MAIN_PROMPT = """
[INST]
I have a topic that contains the following documents:
[DOCUMENTS]

The topic is described by the following keywords: '[KEYWORDS]'.

Based on the information about the topic above, please create a short label of this topic. Make sure you to only return the label and nothing more.
[/INST]
"""


def representation_model(nr_docs=10):
    # Load quantized model.
    bnb_config = BitsAndBytesConfig(
        load_in_4bit=True,  # 4-bit quantization
        bnb_4bit_quant_type='nf4',  # Normalized float 4
        bnb_4bit_use_double_quant=True,  # Second quantization after the first
        bnb_4bit_compute_dtype=torch.bfloat16  # Computation type
    )

    llama_labeling_tokenizer = AutoTokenizer.from_pretrained(
        LLAMA_LABELING_MODEL)

    llama_labeling_model = AutoModelForCausalLM.from_pretrained(
        LLAMA_LABELING_MODEL,
        device_map='auto',
        trust_remote_code=True,
        quantization_config=bnb_config,
    )
    llama_labeling_model.eval()

    llama_generator = pipeline(
        task='text-generation',
        model=llama_labeling_model,
        tokenizer=llama_labeling_tokenizer,
        max_new_tokens=500,
        temperature=0.1,
        repetition_penalty=1.1,
    )

    prompt = LLAMA_SYSTEM_PROMPT + LLAMA_EXAMPLE_PROMPT + LLAMA_MAIN_PROMPT

    meta = {
        'type': 'llama',
        'model': LLAMA_LABELING_MODEL,
        'prompt': prompt,
        'nr_docs': nr_docs,
    }

    return TextGeneration(
        llama_generator,
        prompt=prompt,
        diversity=0.1,
        nr_docs=nr_docs), meta
