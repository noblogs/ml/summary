from bertopic.representation import TextGeneration
from transformers import pipeline
import torch


DEFAULT_LABELING_MODEL = 'google/flan-t5-base'

REPRESENTATION_PROMPT = """Summarize the topic described by these keywords: [KEYWORDS]"""


def representation_model(model=DEFAULT_LABELING_MODEL, nr_docs=5):
    return TextGeneration(
        pipeline(
            'text2text-generation',
            model=model,
            max_new_tokens=100,
            device_map='auto'),
        prompt=REPRESENTATION_PROMPT,
        nr_docs=nr_docs), {
            'type': 'flan',
            'model': model,
            'prompt': REPRESENTATION_PROMPT,
            'nr_docs': nr_docs,
        }
