import os
from llama_cpp import Llama
from bertopic.representation import LlamaCPP

DEFAULT_MODEL = "~/.cache/Llama-3.2-3B-Instruct-Q5_K_S.gguf"

PROMPT = """
I have a topic that contains the following documents:
[DOCUMENTS]

The topic is described by the following keywords: '[KEYWORDS]'.

Based on the above information, provide a short label describing the topic. The label should be in English. Make sure you return the label and nothing else. Do not output any introductory text.
"""


def representation_model(model_path=None, nr_docs=10):
    if not model_path:
        model_path = DEFAULT_MODEL
    model_path = os.path.expanduser(model_path)

    llm = Llama(
        model_path=model_path,
        chat_format='llama-3',
        n_gpu_layers=-1,
        n_ctx=131072,
        pipeline_kwargs={
            'stop': ["\n"],
        },
        verbose=False)

    meta = {
      'type': 'llamacpp',
      'model': os.path.basename(model_path),
      'prompt': PROMPT,
      'nr_docs': nr_docs,
    }

    return LlamaCPP(
            llm,
            prompt=PROMPT,
            nr_docs=nr_docs,
            diversity=0.1,
        ), meta
