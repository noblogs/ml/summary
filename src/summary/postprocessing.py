import re


_please_note_rx = re.compile(r'\s(Please n|N)ote[: ].*$')
_label_rx = re.compile(r'^(Topic *)?Label(ed Topic)?:\s*', re.IGNORECASE)


def postprocess_llm_topic(s):
    """Post-process a LLM-generated topic representation."""

    # Keep only the first line of output.
    n = s.find('\n')
    if n > 0:
        s = s[:n]

    # Remove commonly seen "extraneous information" patterns.
    s = _please_note_rx.sub('', s).strip()
    s = _label_rx.sub('', s)

    # Remove superfluous quoting.
    if s.startswith('"'):
        s = s.strip('"')

    return s
