from collections import namedtuple
import importlib

from bertopic import BERTopic
from bertopic.representation import KeyBERTInspired
from bertopic.vectorizers import ClassTfidfTransformer
from sklearn.feature_extraction.text import CountVectorizer

import nltk
from nltk.corpus import stopwords


DEFAULT_NUM_NEIGHBORS = 30
DEFAULT_NUM_COMPONENTS = 5
DEFAULT_MIN_DIST = 0.02
DEFAULT_UMAP_METRIC = 'cosine'
DEFAULT_MIN_CLUSTER_SIZE = 40


Params = namedtuple(
    'Params',
    ['umap_num_neighbors', 'umap_num_components', 'umap_min_dist',
     'umap_metric', 'hdbscan_min_cluster_size'])

default_params = Params(
    umap_num_neighbors=DEFAULT_NUM_NEIGHBORS,
    umap_num_components=DEFAULT_NUM_COMPONENTS,
    umap_min_dist=DEFAULT_MIN_DIST,
    umap_metric=DEFAULT_UMAP_METRIC,
    hdbscan_min_cluster_size=DEFAULT_MIN_CLUSTER_SIZE)


# Lazily import UMAP and HDBSCAN modules, optionally using CUML.
def _import_umap_hdbscan(with_cuml=True):
    if with_cuml:
        try:
            # run on GPU
            hdbscan = importlib.import_module('cuml.cluster').HDBSCAN
            umap = importlib.import_module('cuml.manifold').UMAP
            normalize = importlib.import_module('cuml.preprocessing').normalize
            print('enabled GPU acceleration for UMAP and HDBSCAN')

            def _hdbscan(**kwargs):
                kwargs['gen_min_span_tree'] = True
                return hdbscan(**kwargs)

            return _hdbscan, umap, normalize
        except ImportError:
            pass

    hdbscan = importlib.import_module('hdbscan').HDBSCAN
    umap = importlib.import_module('umap').UMAP

    def _normalize(x):
        return x

    return hdbscan, umap, _normalize


def _all_stopwords():
    """Return a list of stopwords in all languages supported by NLTK."""
    nltk.download('stopwords')

    multilang_stopwords = set()
    for lang in stopwords.fileids():
        multilang_stopwords.update(stopwords.words(lang))
    return list(sorted(multilang_stopwords))


def analyze_cluster(corpus, params, embedding_model, labeling_model, embeddings,
                    compute_probabilities=False):
    # FIXME: Re-enable CUML once we figure out why it passes tests but
    # has lackluster results with real-world data...
    hdbscan, umap, normalize_embeddings = _import_umap_hdbscan(with_cuml=False)

    if params is None:
        params = default_params

    # Step 2 - Reduce dimensionality
    umap_args = {
        'n_neighbors': params.umap_num_neighbors,
        'n_components': params.umap_num_components,
        'min_dist': params.umap_min_dist,
        'metric': params.umap_metric,
    }
    umap_model = umap(**umap_args)

    # Step 3 - Cluster reduced embeddings
    hdbscan_args = {
        'min_cluster_size': params.hdbscan_min_cluster_size,
        'metric': 'euclidean',
        'cluster_selection_method': 'eom',
        'prediction_data': False,
    }
    hdbscan_model = hdbscan(**hdbscan_args)

    # Step 4 - Tokenize topics, use 1-2 N-Grams.
    vectorizer_model = CountVectorizer(
        stop_words=_all_stopwords(),
        max_features=10000,
        ngram_range=(1, 2))

    # Step 5 - Create topic representation
    ctfidf_model = ClassTfidfTransformer(reduce_frequent_words=True)

    # Step 6 - Fine-tune topic representations
    representation_model = {
        'Main': KeyBERTInspired(nr_repr_docs=10),
        'LLM': labeling_model,
    }

    topic_model = BERTopic(
        verbose=True,
        calculate_probabilities=compute_probabilities,
        embedding_model=embedding_model,
        umap_model=umap_model,
        hdbscan_model=hdbscan_model,
        vectorizer_model=vectorizer_model,
        ctfidf_model=ctfidf_model,
        representation_model=representation_model,
        nr_topics='auto',
    )

    topics, probs = topic_model.fit_transform(
        corpus, normalize_embeddings(embeddings))

    return topic_model, topics, probs
