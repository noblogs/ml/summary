from sentence_transformers import SentenceTransformer
import torch


DEFAULT_EMBEDDING_MODEL = 'paraphrase-multilingual-mpnet-base-v2'


def _finetune_nv_embed_v2(model):
    model.max_seq_length = 32768
    model.tokenizer.padding_side = "right"


_model_customizations = {
    'nvidia/NV-Embed-v2': {
        'params': {
            'model_kwargs': {'torch_dtype': torch.float16},
        },
        'finetune': _finetune_nv_embed_v2,
        'add_eos': True,
        'normalize': True,
    },
}


def load_embedding_model(embedding_model):
    """Load a sentence-transformers embedding model.

    Also applies any relevant hard-coded customizations.
    """
    print(f'loading embedding model {embedding_model}')

    config = _model_customizations.get(embedding_model, {})
    params = {
        'trust_remote_code': True,
    }
    params.update(config.get('params', {}))

    embedding_model_ = SentenceTransformer(
        embedding_model, **params)

    if 'finetune' in config:
        config['finetune'](embedding_model_)

    if config.get('add_eos'):
        embedding_model_ = _AddEOS(embedding_model_)
    if config.get('normalize'):
        embedding_model_ = _Normalize(embedding_model_)

    return embedding_model_


def _encode_single(embedding_model_, corpus, **kwargs):
    return embedding_model_.encode(corpus, **kwargs)


def _encode_multi(embedding_model_, corpus, **kwargs):
    pool = embedding_model_.start_multi_process_pool()
    embeddings = embedding_model_.encode_multi_process(corpus, pool, **kwargs)
    embedding_model_.stop_multi_process_pool(pool)
    return embeddings


def compute_embeddings(docs, embedding_model, parallel=False, **kwargs):
    """Compute sentence embeddings using a sentence-transformers model."""

    encode_func = _encode_multi if parallel else _encode_single

    return encode_func(
        embedding_model, docs,
        show_progress_bar=True,
        **kwargs)


class _AddEOS():

    def __init__(self, embedding_model):
        self._model = embedding_model

    def _add_eos(self, corpus):
        return [x + self._model_.tokenizer.eos_token for x in corpus]

    def encode(self, corpus, **kwargs):
        return self._model.encode(self._add_eos(corpus), **kwargs)

    def encode_multi_process(self, corpus, pool, **kwargs):
        return self._model.encode_multi_process(
            self._add_eos(corpus), pool, **kwargs)


class _Normalize():

    def __init__(self, embedding_model):
        self._model = embedding_model

    def encode(self, corpus, **kwargs):
        kwargs['normalize_embeddings'] = True
        return self._model.encode(corpus, **kwargs)

    def encode_multi_process(self, corpus, pool, **kwargs):
        kwargs['normalize_embeddings'] = True
        return self._model.encode_multi_process(corpus, pool, **kwargs)
