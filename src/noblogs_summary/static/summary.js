import { template } from 'dot'

document.addEventListener('DOMContentLoaded', function() {

    // Load model metadata from the document itself.
    var metadata = {};
    var version = '0';

    // Simple SVG loader.
    var loadingSVG = '<svg class="loading" width="24" height="24" viewBox="0 0 24 24" stroke="white" fill="white" xmlns="http://www.w3.org/2000/svg"><style>.spinner_b2T7{animation:spinner_xe7Q .8s linear infinite}.spinner_YRVV{animation-delay:-.65s}.spinner_c9oY{animation-delay:-.5s}@keyframes spinner_xe7Q{93.75%,100%{r:3px}46.875%{r:.2px}}</style><circle class="spinner_b2T7" cx="4" cy="12" r="3"/><circle class="spinner_b2T7 spinner_YRVV" cx="12" cy="12" r="3"/><circle class="spinner_b2T7 spinner_c9oY" cx="20" cy="12" r="3"/></svg>';

    // Common template modules.
    var tpldefs = {
        closeBtn: document.getElementById('closeButtonTemplate').innerHTML,
    }

    // Templates (doT, extracted from the document itself).
    var templates = {
        error: template(document.getElementById('errorTemplate').innerHTML, undefined, tpldefs),
        topic: template(document.getElementById('topicTemplate').innerHTML, undefined, tpldefs),
        topicList: template(document.getElementById('topicListTemplate').innerHTML, undefined, tpldefs),
        aboutModal: template(document.getElementById('aboutModalTemplate').innerHTML, undefined, tpldefs),
        infoModal: template(document.getElementById('infoModalTemplate').innerHTML, undefined, tpldefs),
    };

    var topics_pane = document.getElementById('topicsPane');
    var content_pane = document.getElementById('contentPane');

    // Show a modal window with contents determined by the template.
    var showModal = function(el, template, obj) {
        let overlay = document.getElementById('modalOverlay');
        overlay.style.display = 'block';
        console.log('showing modal');
        el.innerHTML = template(obj);
        el.querySelectorAll('.close-button').forEach(clel => {
            clel.addEventListener('click', function() {
                el.style.display = 'none';
                overlay.style.display = 'none';
            });
        });
        el.style.display = 'block';
    }

    // Fetch a JSON URL and load template(result) into 'el'.
    var loadJSONInto = function(uri, el, template, obj) {
        console.log('fetching ' + uri);
        el.innerHTML = loadingSVG;
        return fetch(uri)
            .then(response => {
                if (response.status == 409) {
                    console.log('current version ' + version + ' went out of scope, reloading...');
                    reinit();
                    throw new Error('Reloading application data...');
                }
                if (!response.ok) {
                    throw new Error(`${response.status} ${response.statusText}`);
                }
                return response.json();
            })
            .then(result => {
                console.log('result: ', result);
                obj['result'] = result;
                el.innerHTML = template(obj);
            })
            .catch(err => {
                console.log('error: ' + err.message);
                el.innerHTML = templates.error(err);
            });
    }

    var loadAllTopics = function() {
        loadJSONInto('/json/topics/' + version, topics_pane, templates.topicList, {})
            .then(function() {
                setupTopicLinks(topics_pane);
            });
    };

    var setupTopicLinks = function(root_el) {
        root_el.querySelectorAll('.topic-link').forEach(el => {
            el.addEventListener('click', function() {
                let topic_id = el.dataset.topicId;
                let topic_uri = '/json/topic/' + version + '/' + topic_id;
                console.log('clicked topic ' + topic_id);
                loadJSONInto(topic_uri, content_pane, templates.topic, {})
                    .then(function() {
                        setupKeywordLinks(content_pane);
                    });
                return false;
            });
        });
        root_el.querySelectorAll('.close-button').forEach(el => {
            el.addEventListener('click', function() {
                loadAllTopics();
                return false;
            });
        });
    };

    var setupKeywordLinks = function(root_el) {
        root_el.querySelectorAll('.keyword-link').forEach(el => {
            el.addEventListener('click', function() {
                let q = el.dataset.keyword;
                let uri = '/json/query/' + version + '?q=' + encodeURIComponent(q);
                console.log('searching for: ' + q);
                loadJSONInto(uri, topics_pane, templates.topicList, {q: q})
                    .then(function() {
                        setupTopicLinks(topics_pane);
                    });
                return false;
            });
        });
        root_el.querySelectorAll('.close-button').forEach(el => {
            el.addEventListener('click', function() {
                root_el.innerHTML = '';
                return false;
            });
        });
    };

    var reinit = function() {
        fetch('/json/live-version')
            .then(response => {
                if (!response.ok) {
                    throw new Error('error retrieving model metadata: ' + response);
                }
                return response.json();
            })
            .then(result => {
                console.log('received new metadata:', result);
                metadata = result;
                version = metadata.version;
                if (!version) {
                    throw new Error('null version received');
                }

                content_pane.innerHTML = '';
                loadAllTopics();
            })
            .catch(err => {
                console.log('error: ' + err.message);
                topics_pane.innerHTML = templates.error(
                    'There has been an error updating the application data. Please reload the page.');
            });
    };

    // Set up modal window buttons.
    var modal = document.getElementById('modal');
    document.getElementById('infoBtn').addEventListener('click', function() {
        showModal(modal, templates.infoModal, metadata);
    });
    document.getElementById('aboutBtn').addEventListener('click', function() {
        showModal(modal, templates.aboutModal, {});
    });

    // Bootstrap the application, loading all topics.
    reinit();

});

