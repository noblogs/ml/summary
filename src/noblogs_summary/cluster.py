"""Run BERTopic analysis on Noblogs dataset.

References:

* https://maartengr.github.io/BERTopic/index.html
* https://archive.is/58NNS

"""

import argparse
import time

from summary import clustering
from summary.data import Dataset
from summary.embeddings import load_embedding_model
from summary.labeling import load_labeling_model

import numpy
import huggingface_hub
import torch
import torch.cuda


def sorted_topic_posts_map(posts_map, probabilities):
    """Compute a topic/post map based on the aggregate probabilities
    (compute_probabilities=True)."""

    # transpose probabilities to get a topic/sentence matrix of
    # probability values.
    tp = numpy.transpose(probabilities)
    topic_post_map = {}
    for topic_id in range(tp.shape[0]):
        # resolve sentences to posts, keeping the max score
        sentence_scores = sorted(
            enumerate(tp[topic_id, :]), key=lambda x: x[1], reverse=True)
        tmp = {}
        for idx, score in sentence_scores:
            if score < 0.5:
                break
            post_id = posts_map[idx]
            if post_id not in tmp or tmp[post_id] < score:
                tmp[post_id] = score
        posts = sorted(tmp.items(), key=lambda x: x[1], reverse=True)
        topic_post_map[topic_id] = posts
    return topic_post_map


def _normalize_distribution(dist):
    if dist:
        sum_ = sum(dist.values())
    else:
        sum_ = 1.0
    if sum_ == 0:
        sum_ = 1.0
    return dict((x, y/sum_) for x, y in dist.items())


def _sort_tuples_by_value_desc(values):
    return sorted(values, reverse=True, key=lambda x: x[1])


def build_topic_posts_map(posts_map, sentence_topic_map, probabilities):
    """Compute a topic/post map based on topic assignments and
    associated probabilities.

    Works with probabilities of the assigned topic only
    (compute_probabilities=False).

    """
    # First we need to map sentences to posts, we do this by looking
    # at the distribution of probabilities of topic-by-sentence for
    # each post and selecting the highest scoring one. Keep track of
    # the topic ID range for array construction later.
    tmp = {}
    max_topic_id = 0
    for sentence_id, topic_id in enumerate(sentence_topic_map):
        post_id = posts_map[sentence_id]
        prob = probabilities[sentence_id]
        dist = tmp.setdefault(post_id, {})
        if topic_id in dist:
            dist[topic_id] += prob
        else:
            dist[topic_id] = prob
        if topic_id > max_topic_id:
            max_topic_id = topic_id

    # Select the 'best' topic for each post.
    topic_post_map = {}
    for post_id, dist in tmp.items():
        dist = _normalize_distribution(dist)
        sorted_topics_prob = _sort_tuples_by_value_desc(dist.items())
        topic_id, prob = sorted_topics_prob[0]
        topic_post_map.setdefault(topic_id, []).append((post_id, prob))

    # Sort post list for each topic. It's best to create an array
    # since topic_ids are dense, and JSON maps must have string keys.
    # This way we also ignore topic -1 (the "undefined topic").
    out = []
    for i in range(max_topic_id+1):
        out.append(_sort_tuples_by_value_desc(topic_post_map.get(i, [])))
    return out


def _get_device():
    if torch.has_cuda:
        return torch.cuda.get_device_name(0)
    return "CPU"


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--input')
    parser.add_argument('--output')
    parser.add_argument('--huggingface-token')

    # Input filtering.
    parser.add_argument('--ignore-blog-id', type=int, metavar='ID',
                        action='append', default=[17869],
                        help='ignore blogs by ID (repeatable)')
    parser.add_argument('--ignore-blog', metavar='NAME',
                        action='append', default=[],
                        help='ignore blogs by name (repeatable)')

    # Embeddings.
    parser.add_argument('--embeddings')

    # Clusterization (UMAP/HDBSCAN).
    parser.add_argument('--num-neighbors', type=int,
                        default=clustering.DEFAULT_NUM_NEIGHBORS)
    parser.add_argument('--num-components', type=int,
                        default=clustering.DEFAULT_NUM_COMPONENTS)
    parser.add_argument('--umap-min-dist', type=float,
                        default=clustering.DEFAULT_MIN_DIST)
    parser.add_argument('--umap-metric',
                        default=clustering.DEFAULT_UMAP_METRIC)
    parser.add_argument('--min-cluster-size', type=int, metavar='N',
                        default=clustering.DEFAULT_MIN_CLUSTER_SIZE)

    # Labeling.
    parser.add_argument('--labeling-model', default='llamacpp')
    parser.add_argument('--labeling-model-path')
    parser.add_argument('--nr-docs', type=int, default=3)

    parser.add_argument('--topic-map-style', default='default')

    args = parser.parse_args()
    if not args.input or not args.output:
        parser.error('Must specify --input and --output')

    if args.huggingface_token:
        huggingface_hub.login(args.huggingface_token)

    # Load pre-computed embeddings, or compute them as we go.
    if not args.embeddings:
        parser.error('Must specify --embeddings')
    emb_ds = Dataset(args.embeddings)
    posts = emb_ds.posts
    corpus = emb_ds.corpus
    posts_map = emb_ds.posts_map
    embedding_model_name = emb_ds.metadata['embedding_model']

    embedding_model = load_embedding_model(embedding_model_name)
    embeddings = emb_ds.embeddings

    params = clustering.Params(
        umap_num_neighbors=args.num_neighbors,
        umap_num_components=args.num_components,
        umap_min_dist=args.umap_min_dist,
        umap_metric=args.umap_metric,
        hdbscan_min_cluster_size=args.min_cluster_size)

    labeling_model_args = {'nr_docs': args.nr_docs}
    if args.labeling_model_path:
        labeling_model_args['model_path'] = args.labeling_model_path
    labeling_model, labeling_model_meta = load_labeling_model(
        args.labeling_model, **labeling_model_args)

    topic_model, topics, probs = clustering.analyze_cluster(
        corpus,
        params,
        embedding_model,
        labeling_model,
        embeddings,
        compute_probabilities=(args.topic_map_style != 'default'),
    )

    print(topic_model.get_topic_info()[1:11])

    metadata = {
        "embedding_model": embedding_model_name,
        "labeling_model": labeling_model_meta,
        "clustering_params": params._asdict(),
        "device": _get_device(),
        "timestamp": time.time(),
    }

    if args.topic_map_style == 'aggr':
        topic_posts_map = sorted_topic_posts_map(posts_map, probs)
    else:
        topic_posts_map = build_topic_posts_map(
            posts_map, topics, probs)

    # Drop the embeddings.
    ds = Dataset(
        metadata=metadata,
        topic_model=topic_model,
        corpus=corpus,
        posts=posts,
        posts_map=posts_map,
        topic_posts_map=topic_posts_map,
    )

    ds.save(args.output)


if __name__ == '__main__':
    main()
