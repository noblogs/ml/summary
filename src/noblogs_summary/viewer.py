import argparse
import re
import os
from functools import wraps
from flask import Flask, abort, make_response, render_template, request, jsonify
from flask_httpauth import HTTPBasicAuth

from summary.data import Dataset


def cache(max_age=86400):
    def _cache_decorator(fn):
        @wraps(fn)
        def _cache(*args, **kwargs):
            resp = fn(*args, **kwargs)
            resp.headers['Cache-Control'] = (
                f'max-age={max_age}' if resp.status_code == 200 else 'no-store')
            return resp
        return _cache
    return _cache_decorator


def no_cache(fn):
    @wraps(fn)
    def _cache(*args, **kwargs):
        resp = fn(*args, **kwargs)
        resp.headers['Cache-Control'] = 'no-store'
        resp.headers['Expires'] = '-1'
        return resp
    return _cache


def _blog_name(post):
    # Get the blog name from the blog URL.
    m = re.match(r'^https?://([^.]+)\.', post['url'])
    return m[1]


def _process_post(post, score):
    # Transform 'post' objects for JSON output, without the huge
    # 'content' field, and with some additional useful attributes.
    post = dict((k, v) for k, v in post.items() if k != 'content')
    post['blog_name'] = _blog_name(post)
    post['score'] = score
    return post


class Viewer():

    def __init__(self, topics):
        self.topics = topics
        self.topic_post_map = topics.topic_posts_map

    def _get_posts(self, tid):
        return [_process_post(self.topics.posts[x], score)
                for x, score in self.topic_post_map[tid]]

    def get_topics(self):
        return self._to_meta(self.topics.topic_model.get_topics(), with_posts=False)

    def query(self, q, n=5):
        return self._to_meta(
            self.topics.topic_model.find_topics(q, top_n=n)[0])

    def get_topic(self, tid):
        return self._to_meta([tid])[0]

    def _to_meta(self, tids, with_posts=True):
        out_topics = []
        for t in tids:
            if t < 0:
                continue
            topic_info = self.topics.topic_model.get_topic_info(t)
            topic = {
                'id': t,
                'keywords': list(topic_info['Representation'][0]),
                'summary': topic_info['LLM'][0][0],
            }
            if with_posts:
                topic['posts'] = self._get_posts(t)
            out_topics.append(topic)
        return out_topics


def create_viewer_app(topics, live_version='1'):
    viewer = Viewer(topics)

    metadata = {'version': live_version}
    metadata.update(topics.metadata)
    del metadata['labeling_model']['prompt']  # TMI

    app = Flask(__name__)
    app.config.from_envvar('APP_CONFIG', silent=True)

    auth = HTTPBasicAuth()

    @auth.verify_password
    def verify_password(username, password):
        if 'AUTH_USERS' not in app.config:
            return username
        if username in app.config['AUTH_USERS']:
            if password == app.config['AUTH_USERS'][username]:
                return username

    def check_version(fn):
        @wraps(fn)
        def _check_version(version, *args, **kwargs):
            if version != live_version:
                abort(409)
            return fn(version, *args, **kwargs)
        return _check_version

    @app.route('/json/topic/<version>/<int:id>')
    @auth.login_required
    @check_version
    @cache()
    def json_topic(version, id):
        return jsonify(viewer.get_topic(id))

    @app.route('/json/topics/<version>')
    @auth.login_required
    @check_version
    @cache()
    def json_topics(version):
        return jsonify(viewer.get_topics())

    @app.route('/json/query/<version>')
    @auth.login_required
    @check_version
    @cache()
    def json_query(version):
        return jsonify(viewer.query(request.args.get('q')))

    @app.route('/json/live-version')
    @auth.login_required
    @no_cache
    def json_live_version():
        return jsonify(metadata)

    @app.route('/')
    @auth.login_required
    @cache(max_age=1800)
    def index():
        return make_response(
            render_template('index.html',
                            metadata=metadata))

    return app


def print_stats(topics, args):
    topic_post_map = topics.topic_posts_map

    for topic, posts in enumerate(topic_post_map):
        print(f'\n### t={topic} ({len(posts)} posts)')
        topic = int(topic)
        if topic < 0:
            continue
        topic_info = topics.topic_model.get_topic_info(topic)
        kbert_topic = list(topic_info['Representation'][0])
        llm_topic = topic_info['LLM'][0][0]
        print(f'\tkbert={kbert_topic}')
        print(f'\tllm={llm_topic}')

        for post_id, score in posts:
            post = topics.posts[post_id]
            print('    * (%.2f) [%d] %s' % (score, post['blog_id'], post['title']))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--input')
    parser.add_argument('--version', default='0')
    parser.add_argument('--print', action='store_true')
    parser.add_argument('--port', default=8050)
    args = parser.parse_args()
    if not args.input:
        parser.error('Must specify --input')

    topics = Dataset(args.input)

    if args.print:
        print_stats(topics, args)
    else:
        app = create_viewer_app(topics, args.version)
        app.run(port=args.port, debug=False, use_reloader=False)


def main_wsgi():
    return create_viewer_app(
        Dataset(os.getenv('INPUT_PATH')),
        os.getenv('VERSION'),
    )


if __name__ == '__main__':
    main()
else:
    application = main_wsgi()
