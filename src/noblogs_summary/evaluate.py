import argparse
from summary.data import Dataset


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--input')
    args = parser.parse_args()
    if not args.input:
        parser.error('Must specify --input')

    topics = Dataset(args.input)

    # Print the ratio of the count of posts in the first topic to the
    # count of posts in the second one. This is a very approximate
    # measure of the "steepness" of the distribution, which we can use
    # to figure out if the clustering went wrong and put everything in
    # a single giant topic.
    counts = topics.topic_model.get_topic_info()[1:11]['Count']
    ratio = counts[1] / counts[2]
    print(ratio)


if __name__ == '__main__':
    main()
