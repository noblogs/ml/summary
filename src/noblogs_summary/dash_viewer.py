import argparse
from dash import Dash, dcc, html

from noblogs_summary.data import Dataset


def create_dash_app(topics, show_documents):
    # Start a Dash app with the topic visualization.
    if show_documents:
        fig = topics.topic_model.visualize_documents(
            topics.corpus, embeddings=topics.embeddings)
    else:
        fig = topics.topic_model.visualize_topics()

    app = Dash()
    app.layout = html.Div([
        dcc.Graph(figure=fig)
    ])

    return app


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--input')
    parser.add_argument('--port', default=8050)
    parser.add_argument('--documents', action='store_true')
    args = parser.parse_args()
    if not args.input:
        parser.error('Must specify --input')

    topics = Dataset(args.input)

    app = create_dash_app(topics, args.documents)
    app.run(port=args.port, debug=False, use_reloader=False)


if __name__ == '__main__':
    main()
