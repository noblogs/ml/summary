import json
import re
from summary.data import open_compressed_file
from .textproc import strip_more, strip_urls


def read_jsonl(path):
    with open_compressed_file(path) as fd:
        for line in fd:
            yield json.loads(line)


MIN_POST_CONTENT_LEN = 200
_blog_url_rx = re.compile(r'^https://([^.]+)\.')


def _is_blocked_blog_id(post, blocked_blog_ids):
    return post['blog_id'] in blocked_blog_ids


def _is_blocked_blog_name(post, blocked_blog_names):
    """Test if a blog is in the blocklist, using its URL."""
    m = _blog_url_rx.match(post['url'])
    if not m:
        return False
    return m[1].lower() in blocked_blog_names


def _post_ok(post, blocked_blog_ids, blocked_blog_names):
    """Test if we should include a post in the dataset."""

    # Malformed posts (no title, no content).
    if not post.get('blog_id') or not post.get('content') or not post.get('title'):
        return False

    # Posts whose content is too short.
    if len(post['content']) < MIN_POST_CONTENT_LEN:
        return False

    # Blocklists (by ID and by name).
    if _is_blocked_blog_id(post, blocked_blog_ids):
        return False
    if _is_blocked_blog_name(post, blocked_blog_names):
        return False

    return True


def load_noblogs_posts(path, blocked_blog_ids={}, blocked_blog_names={}):
    """Load posts in JSONL format."""
    print('loading data corpus...')
    return [post for post in read_jsonl(path)
            if _post_ok(post, blocked_blog_ids, blocked_blog_names)]


def _post_content(post):
    return f"{post['title']} {post['content']}"


def posts_to_docs(posts):
    return [strip_more(strip_urls(_post_content(x))) for x in posts]
