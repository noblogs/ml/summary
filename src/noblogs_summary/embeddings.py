import argparse

from summary.data import Dataset
from summary.preprocessing import documents_to_sentences
from summary.embeddings import load_embedding_model, compute_embeddings, \
    DEFAULT_EMBEDDING_MODEL

from noblogs_summary.preprocessing import load_noblogs_posts, posts_to_docs


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--input')
    parser.add_argument('--output')
    parser.add_argument('--ignore-blog-id', type=int, metavar='ID',
                        action='append', default=[17869],
                        help='ignore blogs by ID (repeatable)')
    parser.add_argument('--ignore-blog', metavar='NAME',
                        action='append', default=[],
                        help='ignore blogs by name (repeatable)')
    parser.add_argument('--embedding-model',
                        default=DEFAULT_EMBEDDING_MODEL)
    parser.add_argument('--parallel', action=argparse.BooleanOptionalAction)
    args = parser.parse_args()
    if not args.input or not args.output:
        parser.error('Must specify --input and --output')

    posts = load_noblogs_posts(
        args.input,
        blocked_blog_ids=args.ignore_blog_id,
        blocked_blog_names=args.ignore_blog,
    )

    corpus, posts_map = documents_to_sentences(posts_to_docs(posts))

    embedding_model = load_embedding_model(args.embedding_model)

    embeddings = compute_embeddings(corpus,
                                    embedding_model,
                                    parallel=args.parallel)

    print(f'embeddings shape = {embeddings.shape}')

    ds = Dataset(
        corpus=corpus,
        posts=posts,
        posts_map=posts_map,
        embeddings=embeddings,
        metadata={
            "embedding_model": args.embedding_model,
        },
    )
    ds.save(args.output)


if __name__ == '__main__':
    main()
