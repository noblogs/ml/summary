import re
from nltk.tokenize import sent_tokenize


def chunked_sentence_iterator(s, chunk_size):
    """Break s into sentences, combining them to get strings of
    approximately chunk_size.

    """
    chunks = []
    cur = []
    cur_len = 0

    for sentence in sent_tokenize(s):
        n = len(sentence)
        if n > chunk_size - 1:
            chunks.append(sentence)
            continue
        if cur_len + n <= chunk_size:
            cur.append(sentence)
            cur_len += n + 1
        else:
            chunks.append(' '.join(cur))
            cur = [sentence]
            cur_len = n

    if cur:
        chunks.append(' '.join(cur))

    return chunks


_url_rx = re.compile(r'https?://\S*')
_html_rx = re.compile(r'<[^>]*>')
_embed_rx = re.compile(r'\[/?[a-z][^]]+\]')
_spaces_rx = re.compile(r'\s+')
_more_rx = re.compile(r'\bmore…')


def strip_urls(s):
    """Remove URLs from the text."""
    return _url_rx.sub(' ', s)


def strip_html(s):
    return _html_rx.sub(' ', s)


def strip_more(s):
    return _more_rx.sub(' ', s)


def strip_wordpress_embeds(s):
    return _embed_rx.sub(' ', s)


def collapse_spaces(s):
    return _spaces_rx.sub(' ', s)
