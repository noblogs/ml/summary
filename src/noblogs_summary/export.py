import argparse
import html
import json
import pymysql
import zlib
from datetime import datetime, timedelta
from flask import Flask, request, stream_with_context
from flask_httpauth import HTTPTokenAuth

from noblogs_summary.textproc import collapse_spaces, strip_html, strip_wordpress_embeds


def _plaintext(s):
    return html.unescape(
        collapse_spaces(
            strip_wordpress_embeds(
                strip_html(s))))


def _export(mysql_connection, since_date):
    with mysql_connection:
        with mysql_connection.cursor() as cursor:
            cursor.execute('''
                SELECT
                  a.blog_id AS blog_id, a.post_id AS post_id, a.title AS title, a.author AS author,
                  a.content AS content, a.url AS url, a.thumbnail_url AS thumbnail_url, a.published_at AS published_at
                FROM ai_global_activity AS a
                  JOIN wp_blogs AS b
                  ON a.blog_id = b.blog_id
                WHERE
                  b.public = 1 AND b.spam = 0 AND b.deleted = 0 AND b.mature = 0 AND a.published_at > %s
                ORDER BY published_at ASC
''', since_date)
            for row in cursor.fetchall():
                yield json.dumps({
                    'blog_id': row['blog_id'],
                    'post_id': row['post_id'],
                    'author': row['author'],
                    'title': _plaintext(row['title']),
                    'content': _plaintext(row['content']),
                    'date': row['published_at'].isoformat(),
                    'thumbnail_url': row['thumbnail_url'],
                    'url': row['url'],
                }).encode('utf-8') + b'\n'


def _compress(inp):
    # The chosen 'wbits' value creates a gzip container.
    z = zlib.compressobj(level=6, wbits=31)
    for data in inp:
        yield z.compress(data)
    yield z.flush()


def create_exporter_app():
    app = Flask(__name__)
    app.config.from_envvar('APP_CONFIG', silent=True)

    auth = HTTPTokenAuth(scheme='Bearer')

    @auth.verify_token
    def verify_token(token):
        if token == app.config['AUTH_SECRET']:
            return 'export'

    def _dbconnect():
        return pymysql.connect(
            user=app.config.get('MYSQL_USER', 'root'),
            password=app.config.get('MYSQL_PASSWORD'),
            db=app.config.get('MYSQL_DATABASE', 'noblogs'),
            host=app.config.get('MYSQL_HOST', '127.0.0.1'),
            port=app.config.get('MYSQL_PORT', 3308),
            charset='utf8mb4',
            cursorclass=pymysql.cursors.DictCursor,
        )

    @app.route('/export')
    @auth.login_required
    def index():
        days = int(request.args.get('days', '30'))
        if days <= 0:
            days = 30
        return stream_with_context(
            _compress(_export(
                _dbconnect(),
                datetime.now() - timedelta(days),
            ))), {
                "Cache-Control": "no-store",
                "Content-Type": "application/octet-stream",
            }

    return app


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--port', default=8053)
    args = parser.parse_args()

    app = create_exporter_app()
    app.run(port=args.port, debug=False, use_reloader=False)


def main_wsgi():
    return create_exporter_app()


if __name__ == '__main__':
    main()
else:
    application = main_wsgi()
