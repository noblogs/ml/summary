FROM node:current-bookworm AS assets

WORKDIR /src/noblogs-summary

ADD package.json /src/noblogs-summary/package.json
ADD package-lock.json /src/noblogs-summary/package-lock.json
ADD src /src/noblogs-summary/src/

RUN npm install && \
    ./node_modules/.bin/esbuild --bundle src/noblogs_summary/static/summary.js --minify > src/noblogs_summary/static/summary.min.js

FROM registry.git.autistici.org/noblogs/ml/data-sitter:master AS sitter

FROM registry.git.autistici.org/ai3/docker/uwsgi-base:master

ARG DEBIAN_FRONTEND=noninteractive

WORKDIR /src/noblogs-summary

COPY pyproject.toml /src/noblogs-summary/pyproject.toml
COPY MANIFEST.in /src/noblogs-summary/MANIFEST.in
COPY --from=assets /src/noblogs-summary/src/ /src/noblogs-summary/src/

RUN apt-get -q update && \
    apt-get install -qy --no-install-recommends python3-dev python3-setuptools python3-virtualenv python3-wheel git build-essential zstd tar && \
    /virtualenv/bin/pip3 install '.[gpu]' && \
    apt-get remove -qy build-essential && \
    apt-get autoremove -y && \
    apt-get clean && \
    rm -fr /var/lib/apt/lists/* && \
    rm -fr /root/.cache && \
    mv /etc/services.d/uwsgi/run /etc/services.d/uwsgi/run.real

COPY --from=sitter /data-sitter /data-sitter

COPY conf/ /etc/

ENV VIRTUALENV=/virtualenv
ENV UWSGI_ENGINE=threads
ENV NUMBA_CACHE_DIR=/tmp

#ENV WSGI_APP=noblogs_summary.export:application
ENV WSGI_APP=noblogs_summary.viewer:application
