#!/bin/bash
#
# Run the noblogs-ml analysis pipeline:
# * download source dataset
# * run clusterization
# * upload the new result
#
# Assumes a virtualenv iN $VIRTUALENV with all the necessary
# dependencies, and a compiled data-sitter binary in $DATA_SITTER.

EXPORT_DAYS=60
EXPORT_URL="https://ml-export.noblogs.org/export?days=${EXPORT_DAYS}"
EXPORT_SECRET="$(cat .export_secret)"
UPLOAD_URL="https://ml-coordinator.noblogs.org"
UPLOAD_SECRET="$(cat .upload_secret)"
DATADIR="./versions"
DATA_SITTER="../data-sitter/data-sitter"
CORPUS_NAME="bolle"
NR_DOCS=300

VERSION=$(date +%s)

keep=0
upload=1
skew_threshold=5.0
while [ $# -gt 0 ]; do
  case "$1" in
    --keep) keep=1 ;;
    --no-upload) upload=0 ;;
    --skew-threshold)
      skew_threshold="$2"
      shift
      ;;
    *) echo "Unknown option $1" >&2; exit 2 ;;
  esac
  shift
done

input="${DATADIR}/${VERSION}.input.json.gz"
output="${DATADIR}/${VERSION}"
tmpdir=$(mktemp -d ./.tmp.embeddings.XXXXXXXXXX)
mkdir -p "${DATADIR}" "${output}"
if [ $keep -eq 0 ]; then
  trap "rm -fr ${output} ${tmpdir}" EXIT INT TERM
fi

VIRTUALENV="${VIRTUALENV:-../venv}"

set -e
set -o pipefail
set -x

# Download the latest data dump.
curl -sf -H "Authorization: Bearer ${EXPORT_SECRET}" "${EXPORT_URL}" > "${input}"

# Compute embeddings on GPU.
env TOKENIZERS_PARALLELISM=true PYTHONPATH=. \
    ${VIRTUALENV}/bin/python3 noblogs_summary/embeddings.py \
    --parallel \
    --input="${input}" \
    --output="${tmpdir}/embeddings"

# Run topic analysis.
# Iterate until the evaluation parameter falls below threshold.
#export PYTORCH_CUDA_ALLOC_CONF=max_split_size_mb:128
count=0
evaluate_ok=0
while [ $count -lt 10 ]; do
    count=$(( $count + 1 ))
    env TOKENIZERS_PARALLELISM=true PYTHONPATH=. \
        ${VIRTUALENV}/bin/python3 noblogs_summary/cluster.py \
        --min-cluster-size=40 \
        --nr-docs=${NR_DOCS} \
        --input="${input}" \
        --embeddings="${tmpdir}/embeddings" \
        --output="${output}/noblogs"

    skew=$(env PYTHONPATH=. ${VIRTUALENV}/bin/python3 noblogs_summary/evaluate.py --input="${output}/noblogs")
    echo "topic distribution skew = ${skew}, threshold = ${skew_threshold}"
    if (( $(echo "$skew < $skew_threshold" | bc -l) )); then
        evaluate_ok=1
        break
    fi
done

if [ $evaluate_ok -eq 0 ]; then
  echo "Model failed to satisfy the skew threshold criteria!" >&2
  exit 1
fi

# Create and upload data bundle.
if [ $upload -eq 1 ]; then
  ${DATA_SITTER} upload --coordinator=${UPLOAD_URL} --name=${CORPUS_NAME} --secret=${UPLOAD_SECRET} "${output}"
fi

exit 0
