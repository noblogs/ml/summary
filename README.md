Summary
===

An ML-based *topic analyzer* for Noblogs.org.

The purpose is to present a view of "what is going on on noblogs" that
is algorithmically, rather than manually, curated.

It uses the
[BERTopic](https://maartengr.github.io/BERTopic/index.html) toolkit to
combine ML-based embeddings and more traditional TF-IDF clustering
techniques. The multilingual embeddings allow the clustering model to
"understand" multiple languages transparently.

Once clustering produces a topic, we obtain multiple *representations*
for it:

* a simple "bag of keywords" (chosen via the
  [KeyBERTInspired](https://maartengr.github.io/BERTopic/getting_started/representation/representation.html#keybertinspired)
  algorithm), which identifies the keywords that are most
  characteristic for the topic;
* a one-sentence description, obtained using a chat-oriented text
  generation model (currently FLAN-T5) presented with representative
  documents from the topic.

There is a little bit of complexity in how we build the data for
cluster analysis, as we have to break it down into "chunks" that are
smaller than the machine learning models' maximum token size. We do
this with the help of the NLTK sentence tokenizer (see the
*chunked_sentence_iterator* in textproc.py).

## Local installation

Use a Python virtualenv and pip:

```shell
$ sudo apt install python3-virtualenv
$ virtualenv ./venv
$ ./venv/bin/pip3 install -rrequirements.txt
```

This will take forever and requires about 5G of disk space, due to the
large amount of dependencies. If you have a CUDA development
environment installed, the software will be able to take advantage of
your GPU.

## Running the analysis

Once you have acquired a JSONL dump of the noblogs *global-activity*
table (either via web export, which is not there yet, or via the old
dump-posts.py script), you can run the analysis with:

```shell
$ PYTHONPATH=. ../venv/bin/python3 noblogs_summary/cluster.py --input=posts.json.gz --output=bolle
```

and then run the visualization web server:

```shell
$ PYTHONPATH=. ../venv/bin/python3 noblogs_summary/viewer.py --input=bolle
```
